﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace SearchTree.binary
{
    /// <summary>
    /// Represents a binary tree with sentinel nodes. A sentinel node will be placed to avoid that a leaf node could have a null child. 
    /// Sentinel nodes can simplify tree algorithms.
    /// </summary>
    /// <typeparam name="TKey">Type of the value encapsulated in the node</typeparam>
    /// <typeparam name="TNode">Type of the children nodes</typeparam>
    public abstract class BinTreeSentinel<TKey, TNode, TValue> : BinTree<TKey, TNode, TValue>
        where TKey : IComparable<TKey>
        where TNode : BinTNode<TKey, TNode, TValue>, new()
    {
        /// <summary>
        /// Sentinel node. The sentinel node is a leaf node with level equals to zero
        /// </summary>
        protected TNode sentinel;

        /// <summary>
        /// Creates a void tree
        /// </summary>
        public BinTreeSentinel()
            : base(new TNode())
        {
            this.sentinel = this.root;
        }

        /// <summary>
        /// Creates a void tree with a customized comparer
        /// </summary>
        /// <param name="comparer">Customized comparer</param>
        public BinTreeSentinel(IComparer<TKey> comparer)
            : base(new TNode(), comparer)
        {
            this.sentinel = this.root;
        }

        #region IENUMERABLE

        /// <summary>
        /// Enumerator to get all the elements of the tree. Sentinels must not be returned
        /// </summary>
        /// <returns>Enumerator to get all the elements of the tree.</returns>
        protected override IEnumerator<TNode> getNodesEnumerator()
        {
            IEnumerator<TNode> baseEnumerator = base.getNodesEnumerator();

            while (baseEnumerator.MoveNext()) {
                if (baseEnumerator.Current != sentinel) {
                    yield return baseEnumerator.Current;
                }
            }
        }

        #endregion

        /// <summary>
        /// Clears all the elements of the tree.
        /// </summary>
        public override void Clear()
        {
            this.root = sentinel;
        }

        public override LinkedList<TNode> getViewBetween(TKey minimun, TKey maximun)
        {
            LinkedList<TNode> list = base.getViewBetween(minimun, maximun);

            for (LinkedListNode<TNode> pivot = list.First; pivot != null; pivot = pivot.Next) {
                while (pivot.Value == sentinel) {
                    LinkedListNode<TNode> nextNode = pivot.Next;
                    list.Remove(pivot);
                    pivot = nextNode;

                    if (pivot == null) return list;
                }
            }

            return list;
        }
    }
}
