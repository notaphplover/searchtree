﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: The class has been renamed. The class inherits now from BaseTree<TNode, TValue>. Added properties to the class (implicitly).
==================================*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace SearchTree.binary
{
    /// <summary>
    /// Represents a binary Tree. A binary tree has a root node. The graph asociated with the tree must not have cycles
    /// </summary>
    /// <typeparam name="TKey">Type of the value encapsulated in the node</typeparam>
    /// <typeparam name="TNode">Type of the children nodes</typeparam>
    public abstract class BinTree<TKey, TNode, TValue> : ValueEnumerableTree<TNode, TValue>
        where TKey : IComparable<TKey>
        where TNode : BinTNode<TKey, TNode, TValue>
    {
        #region FIELDS

        /// <summary>
        /// Tree comparer
        /// </summary>
        protected IComparer<TKey> comparer;

        #endregion

        /// <summary>
        /// Creates a new tree with a root.
        /// </summary>
        /// <param name="root">Tree´s root</param>
        public BinTree(TNode root) 
            : base(root)
        {
            this.comparer = Comparer<TKey>.Default;
        }

        /// <summary>
        /// Creates a new tree with a root and a customized comparer.
        /// </summary>
        /// <param name="root">Tree´s root</param>
        /// <param name="comparer">Customized comparer</param>
        public BinTree(TNode root, IComparer<TKey> comparer)
            : base(root)
        {
            if (comparer == null)
                this.comparer = Comparer<TKey>.Default;
            else
                this.comparer = comparer;
        }

        public virtual LinkedList<TNode> getViewBetween(TKey minimun, TKey maximun) {

            Stack<TNode> nodesStack = new Stack<TNode>();
            Stack<Boolean> statusStack = new Stack<Boolean>();

            LinkedList<TNode> collection = new LinkedList<TNode>();

            //Initial load
            nodesStack.Push(root);
            statusStack.Push(false);

            while (nodesStack.Count > 0) {
                TNode top = nodesStack.Peek();
                Boolean topS = statusStack.Peek();

                if (top.left == null || topS) {

                    int minComp = comparer.Compare(minimun, top.key);
                    int maxComp = comparer.Compare(maximun, top.key);

                    nodesStack.Pop();
                    statusStack.Pop();

                    if (maxComp > 0) {
                        if (top.right != null) {
                            nodesStack.Push(top.right);
                            statusStack.Push(false);
                        }

                        if (minComp <= 0) {
                            collection.AddLast(top);
                        }
                    } else {
                        if (maxComp == 0) {
                            collection.AddLast(top);
                        }
                    }

                } else {
                    statusStack.Pop();
                    statusStack.Push(true);

                    if (comparer.Compare(minimun, top.key) < 0) {
                        nodesStack.Push(top.left);
                        statusStack.Push(false);
                    }
                }
            }

            return collection;
        }
    }
}
