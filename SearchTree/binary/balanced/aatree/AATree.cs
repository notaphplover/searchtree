﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: AATree inherits from AATreeBase. Added AATree<Key>.
==================================*/
using System;
using System.Collections.Generic;

namespace SearchTree.binary.balanced
{
    public class AATree<TKey> : AATreeBase<TKey, AANode<TKey>, TKey>
        where TKey : IComparable<TKey>
    {
        /// <summary>
        /// Creates a void AA tree
        /// </summary>
        public AATree()
            : base()
        { }

        /// <summary>
        /// Creates an AATree with a customized comparer
        /// </summary>
        /// <param name="comparer">Customized comparer</param>
        public AATree(IComparer<TKey> comparer)
            : base(comparer)
        { }

        /// <summary>
        /// Inserts an element in the tree
        /// </summary>
        /// <param name="key">Key of the new element</param>
        /// <param name="overwrite">If there is already a node with an equivalent key, the node will be overwritten</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Insert(TKey key, bool overwrite)
        {
            AANode<TKey> inserted = new AANode<TKey>(key, this.sentinel);
            return this.insert(ref this.root, ref inserted, overwrite);
        }

        /// <summary>
        /// Inserts an element in the tree
        /// </summary>
        /// <param name="key">Key of the new element</param>
        /// <param name="overwrite">If there is already a node with an equivalent key, the node will be overwritten</param>
        /// <param name="inserted">Node inserted the tree. If overwrite is false and there is already a node with an equivalent key, this variable will point to that node</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Insert(TKey key, bool overwrite, out AANode<TKey> inserted)
        {
            inserted = new AANode<TKey>(key, this.sentinel);
            return this.insert(ref this.root, ref inserted, overwrite);
        }

        /// <summary>
        /// Deletes an element of the tree
        /// </summary>
        /// <param name="key">Key of the node to be deleted</param>
        /// <param name="deletedKey">Deleted key</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Delete(TKey key, out TKey deletedKey)
        {
            AANode<TKey> deleted = null;
            AANode<TKey> nodeDeleted = null;
            deletedKey = default(TKey);
            bool resultOp = this.delete(ref this.root, ref deleted, key, ref nodeDeleted);

            if (resultOp)
                deletedKey = nodeDeleted.key;

            return resultOp;
        }

        /// <summary>
        /// Search an element of the tree by a key.
        /// </summary>
        /// <param name="key">Key to match</param>
        /// <param name="result">The result of the search will be stored in this parameter</param>
        /// <returns>True if a node was found</returns>
        public bool Search(TKey key, out TKey result) 
        {
            AANode<TKey> node = this.search(root, key);

            if (node == null) {
                result = default(TKey);
                return false;
            } else {
                result = node.key;
                return true;
            }
        }
    }

    public class AATree<TKey, TValue> : AATreeBase<TKey, AANode<TKey, TValue>, TValue>
        where TKey : IComparable<TKey>
    {

        /// <summary>
        /// Creates a void AA tree
        /// </summary>
        public AATree()
            : base()
        { }

        /// <summary>
        /// Creates an AATree with a customized comparer
        /// </summary>
        /// <param name="comparer">Customized comparer</param>
        public AATree(IComparer<TKey> comparer)
            : base(comparer)
        { }

        #region ALGORITHM

        /// <summary>
        /// Inserts an element in the tree
        /// </summary>
        /// <param name="key">Key of the new element</param>
        /// <param name="value">Value of the new element</param>
        /// <param name="overwrite">If there is already a node with an equivalent key, the node will be overwritten</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Insert(TKey key, TValue value, bool overwrite)
        {
            AANode<TKey, TValue> inserted = new AANode<TKey, TValue>(key, value, this.sentinel);
            return this.insert(ref this.root, ref inserted, overwrite);
        }

        /// <summary>
        /// Inserts an element in the tree
        /// </summary>
        /// <param name="key">Key of the new element</param>
        /// <param name="value">Value of the new element</param>
        /// <param name="overwrite">If there is already a node with an equivalent key, the node will be overwritten</param>
        /// <param name="inserted">Node inserted under the target node. If overwrite is false and there is already a node with an equivalent key, this variable will point to that node</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Insert(TKey key, TValue value, bool overwrite, out AANode<TKey, TValue> inserted) 
        {
            inserted = new AANode<TKey, TValue>(key, value, this.sentinel);
            return this.insert(ref this.root, ref inserted, overwrite);
        }

        /// <summary>
        /// Deletes an element of the tree
        /// </summary>
        /// <param name="key">Key of the node to be deleted</param>
        /// <param name="value">The value of the deleted node will be stored here</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Delete(TKey key, out TValue value)
        {
            AANode<TKey, TValue> deleted = null;
            AANode<TKey, TValue> nodeDeleted = null;
            value = default(TValue);
            bool resultOp = this.delete(ref this.root, ref deleted, key, ref nodeDeleted);

            if (resultOp)
                value = nodeDeleted.Value;

            return resultOp;
        }

        /// <summary>
        /// Search an element of the tree by a key.
        /// </summary>
        /// <param name="key">Key to match</param>
        /// <param name="result">The result of the search will be stored in this parameter</param>
        /// <returns>True if a node was found</returns>
        public bool Search(TKey key, out TValue result) 
        {
            AANode<TKey, TValue> node = this.search(root, key);

            if (node == null) {
                result = default(TValue);
                return false;
            } else {
                result = node.value;
                return true;
            }
        }

        #endregion

        /// <summary>
        /// Index accesor to perform searches and insertions
        /// </summary>
        /// <param name="key">Key to match</param>
        /// <returns>Result of the search</returns>
        public TValue this[TKey key] {
            get {
                TValue result;
                if (this.Search(key, out result)) {
                    return result;
                } else {
                    throw new KeyNotFoundException();
                }
            }
            set {
                this.Insert(key, value, true);
            }
        }
    }
}
