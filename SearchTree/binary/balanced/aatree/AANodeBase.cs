﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;

namespace SearchTree.binary.balanced
{
    public abstract class AANodeBase<TKey, TNode, TValue> : BinTNode<TKey, TNode, TValue>
        where TKey : IComparable<TKey>
        where TNode : AANodeBase<TKey, TNode, TValue>
    {
        #region ATTRIBUTES

        // node internal data
        internal int level;

        #endregion

        /// <summary>
        /// Constuctor for the sentinel node
        /// </summary>
        public AANodeBase() 
            : base()
        {
            this.level = 0;
        }

        /// <summary>
        /// Constuctor for regular nodes
        /// </summary>
        /// <param name="key">Key of the node</param>
        /// <param name="value">Value of the node</param>
        /// <param name="sentinel">Sentinel node of the tree</param>
        public AANodeBase(TKey key, TNode sentinel) 
            : base(sentinel, sentinel, key)
        {
            this.level = 1;
        }

        /// <summary>
        /// Method designed to supply the restrictions in the constructor constraints of a generic type.
        /// </summary>
        /// <param name="left">Left link</param>
        /// <param name="right">Right link</param>
        /// <param name="key">Key of the node</param>
        internal override void build(TNode left, TNode right, TKey key)
        {
            this.level = 1;
            base.build(left, right, key);
        }

        /// <summary>
        /// Switch the content of two nodes. Children and parent links won´t be changed
        /// </summary>
        /// <param name="node">Target node to swap with this instance.</param>
        internal virtual void switchNodes(TNode node) 
        {
            TKey pivot = this.key;
            this.key = node.key;
            node.key = pivot;
        }
    }
}
