﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;

/*
 * Code obtained from http://demakov.com/snippets/aatree.html
 */
namespace SearchTree.binary.balanced
{
    public class AANode<TKey> : AANodeBase<TKey, AANode<TKey>, TKey>
        where TKey : IComparable<TKey>
    {
        #region PROPERTIES

        /// <summary>
        /// Node´s inner value
        /// </summary>
        public override TKey Value { get { return this.key; } set { this.key = value; } }

        #endregion

        /// <summary>
        /// Constuctor for the sentinel node
        /// </summary>
        public AANode()
            : base()
        { }

        /// <summary>
        /// Constuctor for regular nodes
        /// </summary>
        /// <param name="key">Key of the node</param>
        /// <param name="value">Value of the node</param>
        /// <param name="sentinel">Sentinel node of the tree</param>
        public AANode(TKey key, AANode<TKey> sentinel)
            : base(key, sentinel)
        { }
    }

    /// <summary>
    /// Node of an AATree
    /// </summary>
    /// <typeparam name="TKey">Search key</typeparam>
    /// <typeparam name="TValue">Value</typeparam>
    public class AANode<TKey, TValue> : AANodeBase<TKey, AANode<TKey, TValue>, TValue>
        where TKey : IComparable<TKey>
    {
        #region ATTRIBUTES

        /// <summary>
        /// Node´s inner value
        /// </summary>
        public TValue value;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Node´s inner value
        /// </summary>
        public override TValue Value { get { return this.value; } set { this.value = value; } }

        #endregion  

        /// <summary>
        /// Constuctor for the sentinel node
        /// </summary>
        public AANode() 
            : base()
        {
            this.value = default(TValue);
        }

        /// <summary>
        /// Constuctor for regular nodes
        /// </summary>
        /// <param name="key">Key of the node</param>
        /// <param name="value">Value of the node</param>
        /// <param name="sentinel">Sentinel node of the tree</param>
        public AANode(TKey key, TValue value, AANode<TKey, TValue> sentinel) 
            : base(key, sentinel)
        {
            this.value = value;
        }

        internal override void switchNodes(AANode<TKey, TValue> node)
        {
            base.switchNodes(node);

            TValue pivot = this.value;
            this.value = node.value;
            node.value = pivot;
        }
    }
}
