﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:

Code adapted from http://demakov.com/snippets/aatree.html

 1. Fixed syntax errors with generics.
 2. Added public methods to use the class.
 3. Added search methods.
 4. Changed remove method to avoid th use of an instance variable
 5. It has been done a refactor process to reuse the code in a future

 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: AATree inherits from AATreeBase. Added AATree<Key>.
==================================*/
using System;
using System.Collections.Generic;

namespace SearchTree.binary.balanced
{
    public abstract class AATreeBase<TKey, TNode, TValue> : BinTreeSentinel<TKey, TNode, TValue>
        where TKey : IComparable<TKey>
        where TNode : AANodeBase<TKey, TNode, TValue>, new()
    {
        #region ATTRIBUTES

        /// <summary>
        /// Number of elements in the tree
        /// </summary>
        protected int count;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Number of elements in the tree
        /// </summary>
        public override int Count { get { return this.count; } }

        #endregion

        /// <summary>
        /// Creates a void AA tree
        /// </summary>
        public AATreeBase()
            : base()
        {
            this.count = 0;
        }

        /// <summary>
        /// Creates an AATree with a customized comparer
        /// </summary>
        /// <param name="comparer">Customized comparer</param>
        public AATreeBase(IComparer<TKey> comparer)
            : base(comparer)
        {
            this.count = 0;
        }

        #region ALGORITHM

        /// <summary>
        /// First rotation of the algorithm
        /// </summary>
        /// <param name="node">Center of the rotation</param>
        private void skew(ref TNode node)
        {
            if (node.left != null && node.level == node.left.level)
            {
                // rotate right
                TNode left = node.left;
                node.left = left.right;
                left.right = node;
                node = left;
            }
        }

        /// <summary>
        /// Second rotation of the algorithm
        /// </summary>
        /// <param name="node">Center of the algorithm</param>
        private void split(ref TNode node)
        {
            if (node.right != null && node.right.right != null && node.right.right.level == node.level)
            {
                // rotate left
                TNode right = node.right;
                node.right = right.left;
                right.left = node;
                node = right;
                ++node.level;
            }
        }

        /// <summary>
        /// Inserts an element under a node
        /// </summary>
        /// <param name="node">Target node</param>
        /// <param name="newNode">Node inserted under the target node. If overwrite is false and there is already a node with an equivalent key, this variable will point to that node</param>
        /// <param name="overwrite">If there is already a node with an equivalent key, the node will be overwritten</param>
        /// <returns>True if the operation could be performed</returns>
        protected bool insert(ref TNode node, ref TNode newNode, bool overwrite)
        {
            if (node == this.sentinel) {
                node = newNode;
                ++this.count;
                return true;
            }

            TKey key = newNode.key;

            int compare = comparer.Compare(key, node.key);

            if (compare < 0) {
                if (!this.insert(ref node.left, ref newNode, overwrite))
                    return false;
            } else if (compare > 0) {
                if (!this.insert(ref node.right, ref newNode, overwrite))
                    return false;
            } else {
                if (overwrite) {
                    TNode left = node.left;
                    TNode right = node.right;
                    node = newNode;
                    node.left = left;
                    node.right = right;
                    return true;
                } else {
                    newNode = node;
                    return false;
                }
            }

            this.skew(ref node);
            this.split(ref node);

            return true;
        }

        /// <summary>
        /// Deletes an element of the tree
        /// </summary>
        /// <param name="key">Key of the node to be deleted</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Delete(TKey key)
        {
            TNode node;
            return this.Delete(key, out node);
        }

        /// <summary>
        /// Deletes an element of the tree
        /// </summary>
        /// <param name="key">Key of the node to be deleted</param>
        /// <param name="node">Deleted node</param>
        /// <returns>True if the operation could be performed</returns>
        public bool Delete(TKey key, out TNode node)
        {
            TNode deleted = null;
            node = default(TNode);
            bool resultOp = this.delete(ref this.root, ref deleted, key, ref node);

            return resultOp;
        }

        /// <summary>
        /// Deletes an element under a node
        /// </summary>
        /// <param name="node">Target node</param>
        /// <param name="deletedPivot">Internal reference to pivot</param>
        /// <param name="key">Key to match</param>
        /// <returns>True if the operation could be performed</returns>
        protected bool delete(ref TNode node, ref TNode deletedPivot, TKey key, ref TNode deleted)
        {
            if (node == sentinel) {
                return (deletedPivot != null);
            }

            int compare = this.comparer.Compare(key, node.key);
            if (compare < 0) {
                if (!this.delete(ref node.left, ref deletedPivot, key, ref deleted))
                    return false;
            } else {
                if (compare == 0)
                    deletedPivot = node;
                
                if (!this.delete(ref node.right, ref deletedPivot, key, ref deleted))
                    return false;
            }

            if (deletedPivot != null) {
                deletedPivot.switchNodes(node);
                deletedPivot = null;
                deleted = node;
                node = node.right;
                deleted.left = deleted.right = this.sentinel;
                --this.count;
            } else if (node.left.level < node.level - 1 || node.right.level < node.level - 1) {
                --node.level;
                if (node.right.level > node.level)
                    node.right.level = node.level;
                this.skew(ref node);
                this.skew(ref node.right);
                this.skew(ref node.right.right);
                this.split(ref node);
                this.split(ref node.right);
            }

            return true;
        }

        /// <summary>
        /// Search an element of the tree by a key.
        /// </summary>
        /// <param name="key">Key to match</param>
        /// <param name="result">The result of the search will be stored in this parameter</param>
        /// <returns>True if a node was found</returns>
        public bool Search(TKey key, out TNode result)
        {
            TNode node = this.search(root, key);

            if (node == null) {
                result = default(TNode);
                return false;
            } else {
                result = node;
                return true;
            }
        }

        /// <summary>
        /// Search an element under a node by a key
        /// </summary>
        /// <param name="node">Target node</param>
        /// <param name="key">Key to match</param>
        /// <returns>True if a node was found</returns>
        protected TNode search(TNode node, TKey key)
        {
            if (node == sentinel) {
                return null;
            } else {
                int comparison = this.comparer.Compare(key, node.key);

                if (comparison < 0) {
                    return this.search(node.left, key);
                } else if (comparison > 0) {
                    return this.search(node.right, key);
                } else {
                    return node;
                }
            }
        }

        #endregion

        #region ICOLLECTION

        public override void Add(TNode node) 
        {
            this.insert(ref this.root, ref node, true);
        }

        public override bool Contains(TNode node)
        {
            TNode searchedNode = this.search(root, node.key);
            return node != null;
        }

        public override bool Remove(TNode node) 
        {
            return this.Remove(node);
        }

        #endregion
    }
}
