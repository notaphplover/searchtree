﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: The class has been renamed. The class inherits now from BaseNode<TNode, TValue>. Added properties to the class.
==================================*/
using System;
using System.Collections.Generic;

namespace SearchTree.binary
{
    /// <summary>
    /// Represents a tree node. A node has a value and two children. A non leaf node must have two different children
    /// </summary>
    /// <typeparam name="TKey">Type of the value encapsulated in the node</typeparam>
    /// <typeparam name="TNode">Type of the children nodes</typeparam>
    public abstract class BinTNode<TKey, TNode, TValue> : BaseNode<TNode, TValue>
        where TNode : BinTNode<TKey, TNode, TValue>
        where TKey : IComparable<TKey>
    {
        #region FIELDS

        /// <summary>
        /// Left link to a node
        /// </summary>
        internal TNode left;

        /// <summary>
        /// Right link to a node
        /// </summary>
        internal TNode right;

        /// <summary>
        /// Value of the node
        /// </summary>
        internal TKey key;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Children nodes.
        /// </summary>
        public override IEnumerable<TNode> Children { 
            get { return new TNode[] { this.left, this.right, }; } 
        }

        /// <summary>
        /// Key of the node
        /// </summary>
        public TKey Key { get { return this.key; } }

        /// <summary>
        /// Left link to a node
        /// </summary>
        public TNode Left { get { return this.left; } }

        /// <summary>
        /// Right link to a node
        /// </summary>
        public TNode Right { get { return this.Right; } }

        #endregion

        /// <summary>
        /// Creates a void node
        /// </summary>
        public BinTNode() {
            this.left = this.right = null;
        }

        /// <summary>
        /// Creates a node with links and a value
        /// </summary>
        /// <param name="left">Left link</param>
        /// <param name="right">Right link</param>
        /// <param name="key">Key of the node</param>
        public BinTNode(TNode left, TNode right, TKey key)
        {
            this.build(left, right, key);
        }

        /// <summary>
        /// Method designed to supply the restrictions in the constructor constraints of a generic type.
        /// </summary>
        /// <param name="left">Left link</param>
        /// <param name="right">Right link</param>
        /// <param name="key">Key of the node</param>
        internal virtual void build(TNode left, TNode right, TKey key)
        {
            this.left = left;
            this.right = right;
            this.key = key;
        }

        /// <summary>
        /// Determines if the node is a leaf node
        /// </summary>
        /// <returns>True if the node is a leaf node</returns>
        public virtual bool isLeaf() {
            //null == null or sentinel == sentinel
            return left == right;
        }
    }
}
