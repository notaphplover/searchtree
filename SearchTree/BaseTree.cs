﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:
==================================*/
using System.Collections;
using System.Collections.Generic;

namespace SearchTree
{
    public abstract class BaseTree<TNode, TValue> : ITree<TNode, TValue>, IEnumerable<TNode>
        where TNode : BaseNode<TNode, TValue>
    {
        #region FIELDS

        /// <summary>
        /// Root node. This node could be a leaf node
        /// </summary>
        protected TNode root;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Number of elements in the tree
        /// </summary>
        public abstract int Count { get; }

        /// <summary>
        /// Root node. This node could be a leaf node
        /// </summary>
        public TNode Root { get { return this.root; } }

        #endregion

        public BaseTree(TNode root)
        {
            this.root = root;
        }

        public BaseTree() : this(default(TNode)) { }

        /// <summary>
        /// Clears all the elements of the tree.
        /// </summary>
        public virtual void Clear()
        {
            this.root = null;
        }

        #region IENUMERABLE

        IEnumerator<TNode> IEnumerable<TNode>.GetEnumerator()
        {
            return this.getNodesEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.getNodesEnumerator();
        }

        protected virtual IEnumerator<TNode> getNodesEnumerator() 
        {
            return this.root.getEnumerator();
        }

        #endregion

        #region ICOLLECTION

        public virtual bool IsReadOnly { get { return false; } }

        public abstract void Add(TNode node);

        public abstract bool Contains(TNode node);

        public virtual void CopyTo(TNode[] array, int statingIndex)
        {
            foreach (TNode element in this) {
                if (statingIndex >= array.Length) return;
                array[statingIndex++] = element;
            }
        }

        public abstract bool Remove(TNode node);
        
        #endregion
    }
}
