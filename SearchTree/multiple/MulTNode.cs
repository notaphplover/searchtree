﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:
==================================*/
using System.Collections.Generic;

namespace SearchTree.multiple
{
    public abstract class MulTNode<TNode, TValue, TCollection> : BaseNode<TNode, TValue>
        where TNode : MulTNode<TNode, TValue, TCollection>
        where TCollection : IEnumerable<TNode>
    {
        #region ATTRIBUTES 

        /// <summary>
        /// Node´s children
        /// </summary>
        internal TCollection children;

        #endregion

        #region PROPERIES

        /// <summary>
        /// Node´s children
        /// </summary>
        public override IEnumerable<TNode> Children
        {
            get { return this.children; }
        }

        #endregion

        public MulTNode()
            :base()
        { }
    }
}
