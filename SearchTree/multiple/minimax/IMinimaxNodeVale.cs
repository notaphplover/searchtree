﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-14
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
namespace SearchTree.multiple.minimax
{
    public interface IMinimaxNodeVale
    {
        /// <summary>
        /// Returns the heuristic value of this node associated with a player
        /// </summary>
        /// <param name="player">Player´s value</param>
        /// <returns>Player´s heuristic value of this node.</returns>
        int getValue(int player);

        /// <summary>
        /// Returns the vector of heuristic values of this node associated with each player.
        /// </summary>
        /// <returns>Vector of heuristic values of this node associated with each player.</returns>
        int[] getValues();
    }
}
