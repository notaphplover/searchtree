﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

      VERSION 1.1: There is no increaseLevels function. Children are created dinamically to increment the efficiency of the AlphaPruneTree
==================================*/
using System;
using System.Collections.Generic;

namespace SearchTree.multiple.minimax.alpha
{
    public abstract class AlphaPruneTreeBase<TNode, TValue> : MinimaxTreeBase<TNode, TValue>
        where TNode : AlphaPruneTreeBaseNode<TNode, TValue>
        where TValue : IMinimaxNodeVale
    {
        public AlphaPruneTreeBase(IMinimaxGenerator<TNode, TValue> generator, TNode root, int players)
            : base(generator, root, players)
        { }

        /*
        public override void increaseLevel(int levels, TNode node)
        {
            base.increaseLevel(levels, node);

            if (levels != 0)
                foreach (TNode child in node.children) {
                    child.Parent = node;
                }
        }
        */

        protected override void getTrace(TNode node, int level, int[] controllers)
        {
            if (level == controllers.Length - 1) {
                node.DeepValues = node.Value.getValues();
            } else {
                node.children = new LinkedList<TNode>();
                node.DeepValues = new int[this.players];

                for (int i = 0; i < this.players; ++i) {
                    node.DeepValues[i] = Int32.MinValue;
                }

                IEnumerator<TNode> childrenEnumerator = this.generator.generateNodes(node);

                int index = 0;

                if (level == 0)
                    while (childrenEnumerator.MoveNext()) {
                        TNode child = childrenEnumerator.Current;
                        child.Parent = node;

                        node.children.AddLast(child);
                        ++this.counter;

                        this.getTrace(child, level + 1, controllers);

                        if (child.DeepValues[controllers[level]] > node.DeepValues[controllers[level]]) {
                            node.DeepValues = child.DeepValues;
                            node.ChildIndex = index;
                        }

                        ++index;
                    }
                else
                    while (childrenEnumerator.MoveNext()) {
                        TNode child = childrenEnumerator.Current;
                        child.Parent = node;

                        node.children.AddLast(child);
                        ++this.counter;

                        this.getTrace(child, level + 1, controllers);

                        if (child.DeepValues[controllers[level]] > node.DeepValues[controllers[level]]) {
                            node.DeepValues = child.DeepValues;
                            node.ChildIndex = index;
                        }

                        //ALPHA PRUNE
                        if (node.DeepValues[controllers[level - 1]] < node.Parent.DeepValues[controllers[level - 1]])
                            return;
                        //END OF ALPHA PRUNE

                        ++index;
                    }
            }
        }

    }
}
