﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
namespace SearchTree.multiple.minimax.alpha
{
    public abstract class AlphaPruneTreeBaseNode<TNode, TValue> : MinimaxTreeNodeBase<TNode, TValue>
        where TNode : AlphaPruneTreeBaseNode<TNode, TValue>
        where TValue : IMinimaxNodeVale
    {
        #region ATTRIBUTES

        /// <summary>
        /// Node´s parent
        /// </summary>
        protected TNode parent;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Node´s parent
        /// </summary>
        public TNode Parent { get { return this.parent; } internal set { this.parent = value; } }

        #endregion

        public AlphaPruneTreeBaseNode(int players)
            : this(players, null)
        { }

        public AlphaPruneTreeBaseNode(int players, TNode parent)
            : base(players)
        {
            this.parent = parent;
        }
    }
}
