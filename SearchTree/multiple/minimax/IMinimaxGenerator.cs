﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-14
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: method generateNodes returns now IEnumerator<TNode>
==================================*/
using System.Collections.Generic;

namespace SearchTree.multiple.minimax
{
    public interface IMinimaxGenerator<TNode, TValue>
        where TNode : MinimaxTreeNodeBase<TNode, TValue>
        where TValue : IMinimaxNodeVale
    {
        IEnumerator<TNode> generateNodes(TNode origin);
    }
}
