﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-14
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

     VERSION 1.1: There is no increaseLevels function. Children are created dinamically to increment the efficiency of the AlphaPruneTree
==================================*/
using System;
using System.Collections.Generic;

using SearchTree.util.extensions;

namespace SearchTree.multiple.minimax
{
    public abstract class MinimaxTreeBase<TNode, TValue> : MulTree<TNode, TValue, LinkedList<TNode>>
        where TNode : MinimaxTreeNodeBase<TNode, TValue>
        where TValue : IMinimaxNodeVale
    {
        #region ATTRIBUTES

        protected int counter;

        protected IMinimaxGenerator<TNode, TValue> generator;

        protected int players;

        #endregion

        #region PROPERTIES

        public override int Count { get { return this.counter; } }

        #endregion

        public MinimaxTreeBase(IMinimaxGenerator<TNode, TValue> generator, TNode root, int players)
            : base(root)
        {
            this.counter = 1;
            this.generator = generator;
            this.players = players;
        }

        #region ICOLLECTION

        public override void Add(TNode node)
        {
            throw new NotImplementedException();
        }

        public override bool Contains(TNode node)
        {
            throw new NotImplementedException();
        }

        public override bool Remove(TNode node)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Returns the sequence of movements assuming that virtual players would play the game
        /// </summary>
        /// <param name="controllers">id of the controling players in each level of the tree</param>
        /// <returns></returns>
        public TNode[] getTrace(int[] controllers) {

            TNode[] trace = new TNode[controllers.Length];

            if (this.root != null)
                this.getTrace(this.root, 0, controllers);

            TNode pivot = this.root;

            for (int i = 0; i < controllers.Length - 1; ++i) {
                trace[i] = pivot;
                pivot = pivot.children.ElementAt(pivot.ChildIndex);
            }

            trace[controllers.Length - 1] = pivot;

            return trace;
        }

        protected virtual void getTrace(TNode node, int level, int[] controllers)
        {
            if (node != null)
                if (level == controllers.Length - 1) {
                    node.DeepValues = node.Value.getValues();
                } else {
                    node.children = new LinkedList<TNode>();
                    node.DeepValues = new int[this.players];

                    for (int i = 0; i < this.players; ++i) {
                        node.DeepValues[i] = Int32.MinValue;
                    }

                    IEnumerator<TNode> childrenEnumerator = this.generator.generateNodes(node);

                    int index = 0;

                    while (childrenEnumerator.MoveNext()) {
                        TNode child = childrenEnumerator.Current;

                        node.children.AddLast(child);
                        ++this.counter;

                        this.getTrace(child, level + 1, controllers);

                        if (child.DeepValues[controllers[level]] > node.DeepValues[controllers[level]]) {
                            node.DeepValues = child.DeepValues;
                            node.ChildIndex = index;
                        }

                        ++index;
                    }
                }
        }

        protected virtual void move(int[] indexes)
        { 
            for (int i = 0; i < indexes.Length; ++i) 
                this.root = this.root.children.ElementAt(indexes[i]);  
        }
    }
}
