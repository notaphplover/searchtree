﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-14
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

      VERSION 1.1: There is no increaseLevels function. Children are created dinamically to increment the efficiency of the AlphaPruneTree
==================================*/
using System.Collections.Generic;

namespace SearchTree.multiple.minimax
{
    public abstract class MinimaxTreeNodeBase<TNode, TValue> : MulTNode<TNode, TValue, LinkedList<TNode>>
        where TNode : MinimaxTreeNodeBase<TNode, TValue>
        where TValue : IMinimaxNodeVale
    {
        #region ATTRIBUTES

        /// <summary>
        /// Players values obtained after a getTrace() call
        /// </summary>
        protected int[] deepValues;

        /// <summary>
        /// Index of the best child after a getTrace() call.
        /// </summary>
        protected int childIndex;

        /// <summary>
        /// Amount of players in the model
        /// </summary>
        protected int players;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Players values obtained after a getTrace() call
        /// </summary>
        public int[] DeepValues { get { return this.deepValues; } internal set { this.deepValues = value; } }

        /// <summary>
        /// Index of the best child after a getTrace() call.
        /// </summary>
        public int ChildIndex { get { return this.childIndex; } internal set { this.childIndex = value; } }

        /// <summary>
        /// Amount of players in the model
        /// </summary>
        public int Players { get { return this.players; } }

        #endregion

        public MinimaxTreeNodeBase(int players)
            : base()
        {
            this.players = players;
        }
    }
}
