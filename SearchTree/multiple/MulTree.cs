﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:
==================================*/
using System.Collections.Generic;

namespace SearchTree.multiple
{
    public abstract class MulTree<TNode, TValue, TCollection> : ValueEnumerableTree<TNode, TValue>
        where TNode : MulTNode<TNode, TValue, TCollection>
        where TCollection : IEnumerable<TNode>
    {
        protected MulTree()
            : base()
        { }

        public MulTree(TNode root)
            : base(root)
        { }
    }
}
