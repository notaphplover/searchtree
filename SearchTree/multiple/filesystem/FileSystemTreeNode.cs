﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-05
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: This class now inherits from FileSystemTreeNodeBase. Algorithms implementation have been moved to that class.
==================================*/
using System;
using System.Collections.Generic;

using SearchTree.binary.balanced;

namespace SearchTree.multiple.filesystem
{
    public class FileSystemTreeNode : FileSystemTreeNodeBase<FileSystemTreeNode, FileSystemNodeValue>
    {
        public FileSystemTreeNode()
            : this(default(FileSystemNodeValue), Comparer<FileSystemTreeNode>.Default)
        { }

        public FileSystemTreeNode(IComparer<FileSystemTreeNode> comparer)
            : this(default(FileSystemNodeValue), comparer)
        { }

        public FileSystemTreeNode(FileSystemNodeValue value)
            : this(value, Comparer<FileSystemTreeNode>.Default)
        { }

        public FileSystemTreeNode(FileSystemNodeValue value, IComparer<FileSystemTreeNode> comparer)
            : base(value, comparer)
        { }
    }
    
}
