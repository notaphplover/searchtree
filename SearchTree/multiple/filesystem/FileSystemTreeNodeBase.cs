﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-12
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using System.Collections.Generic;

using SearchTree.binary.balanced;

namespace SearchTree.multiple.filesystem
{
    public abstract class FileSystemTreeNodeBase<TNode, TValue> : MulTNode<TNode, TValue, AATree<TNode>>, IComparable<TNode>
        where TNode : FileSystemTreeNodeBase<TNode, TValue>
        where TValue : IComparable<TValue>
    {
        #region FIELDS

        /// <summary>
        /// Node´s parent
        /// </summary>
        internal TNode parent;

        /// <summary>
        /// Node´s value
        /// </summary>
        protected TValue value;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Node´s value
        /// </summary>
        public override TValue Value
        {
            get { return this.value; }

            set { this.value = value; }
        }

        #endregion

        public FileSystemTreeNodeBase(TValue value, IComparer<TNode> comparer)
            : base()
        {
            this.children = new AATree<TNode>(comparer);
            this.parent = null;
            this.value = value;
        }

        #region ICOMPARABLE

        /// <summary>
        /// Compares this node with another node.
        /// </summary>
        /// <param name="node">Node to compare with this node</param>
        /// <returns>
        ///     A value less than zero if this instance precedes obj in the sort order.
        ///     Zero if this instance occurs in the same position in the sort order as obj.
        ///     A value greater than zero if this instance follows obj in the sort order.
        ///</returns>
        public int CompareTo(TNode node)
        {
            return this.value.CompareTo(node.value);
        }

        #endregion
    }
}
