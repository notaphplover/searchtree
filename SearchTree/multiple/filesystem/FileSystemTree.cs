﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-05
 SPECIAL NOTES:
 VERSION: 1.2
====================================
 Change History:

    VERSION 1.1: This class now inherits from FileSystemTreeBase. Algorithms implementation have been moved to that class.
    VERSION 1.2: The added a reference to the FIRST_INDEX const.
==================================*/
using System;

namespace SearchTree.multiple.filesystem
{
    public class FileSystemTree : FileSystemTreeBase<FileSystemTreeNode, FileSystemNodeValue>
    {
        public FileSystemTree()
            : this(new FileSystemTreeNode(new FileSystemNodeValue(String.Empty, FileSystemNodeType.INTERMEDIATE)), FileSystemTree.DEFAULT_SEPARATOR)
        { }

        protected FileSystemTree(FileSystemTreeNode node)
            : this(node, FileSystemTree.DEFAULT_SEPARATOR)
        { }

        protected FileSystemTree(char separator)
            : this(new FileSystemTreeNode(new FileSystemNodeValue(String.Empty, FileSystemNodeType.INTERMEDIATE)), separator)
        { }

        protected FileSystemTree(FileSystemTreeNode node, char separator)
            : base(node, separator)
        { }

        #region ALGORITHM

        /// <summary>
        /// Adds a path to the tree. It will be added as many intermediate nodes as needed to add the path.
        /// </summary>
        /// <param name="path">Path to add</param>
        /// <param name="type">Type of the last node</param>
        public virtual bool AddPath(String path, FileSystemNodeType type)
        {
            String[] pathElems = path.Split(this.separator);
            FileSystemTreeNode newNode = new FileSystemTreeNode(new FileSystemNodeValue(pathElems[pathElems.Length - 1], type));
            return this.addPath(this.root, pathElems, FIRST_INDEX, ref newNode);
        }

        /// <summary>
        /// Adds a path to the tree. It will be added as many intermediate nodes as needed to add the path.
        /// </summary>
        /// <param name="path">Path to add</param>
        /// <param name="type">Type of the last node</param>
        /// <param name="inserted">Node inserted the tree. If overwrite is false and there is already a node with an equivalent key, this variable will point to that node</param>
        public virtual bool AddPath(String path, FileSystemNodeType type, ref FileSystemTreeNode inserted)
        {
            String[] pathElems = path.Split(this.separator);
            inserted = new FileSystemTreeNode(new FileSystemNodeValue(pathElems[pathElems.Length - 1], type));
            return this.addPath(this.root, pathElems, FIRST_INDEX, ref inserted);
        }

        #endregion
    }
}
