﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-05
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/

namespace SearchTree.multiple.filesystem
{
    public enum FileSystemNodeType : int
    {
        INTERMEDIATE = 0, //Directories, and soft symbolic links
        LEAF = 1, //Hard links
    }
}
