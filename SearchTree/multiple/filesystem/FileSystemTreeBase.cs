﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-12
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: Added the FIRST_INDEX constant
==================================*/
using System;
using System.Collections.Generic;

using SearchTree.binary.balanced;

namespace SearchTree.multiple.filesystem
{
    public abstract class FileSystemTreeBase<TNode, TValue> : MulTree<TNode, TValue, AATree<TNode>>
        where TNode : FileSystemTreeNodeBase<TNode, TValue>, new()
        where TValue : FileSystemNodeValue, new()
    {
        #region CONSTS

        /// <summary>
        /// Default path separator.
        /// </summary>
        public const char DEFAULT_SEPARATOR = '\\';

        protected const int FIRST_INDEX = 0;

        #endregion

        #region ATTRIBUTES

        /// <summary>
        /// This variable counts the tree nodes inside.
        /// </summary>
        protected int count;

        /// <summary>
        /// Default path separator.
        /// </summary>
        protected char separator;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// This variable counts the tree nodes inside.
        /// </summary>
        public override int Count { get { return this.count; } }

        #endregion

        protected FileSystemTreeBase(TNode node, char separator)
            : base(node)
        {
            this.count = 0;
            this.separator = separator;
        }

        #region IENUMERABLE

        /// <summary>
        /// Obtains all the values inside the tree unless the root´s value.
        /// </summary>
        /// <returns>Values inside the tree unless the root´s value</returns>
        protected override IEnumerator<TValue> getValuesEnumerator()
        {
            foreach(TNode node in this.root.children as IEnumerable<TNode>) {
                IEnumerator<TNode> childEnumerator = node.getEnumerator();

                while (childEnumerator.MoveNext()) {
                    yield return childEnumerator.Current.Value;
                }
            }
        }

        #endregion

        #region ALGORITHM

        /// <summary>
        /// Adds a path to the tree. It will be added as many intermediate nodes as needed to add the path.
        /// </summary>
        /// <param name="node">Node where the current node will be added</param>
        /// <param name="path">Nodes to add</param>
        /// <param name="index">Index of the current node</param>
        /// <param name="newNode">new node to insert</param>
        protected virtual bool addPath(TNode node, String[] path, int index, ref TNode newNode)
        {
            if (index == path.Length - 1) {
                TNode child;
                if (node.children.Search(newNode, out child)) {
                    newNode = child;
                    return false;
                } else {
                    newNode.parent = node;
                    AANode<TNode> inserted;
                    node.children.Insert(newNode, false, out inserted);
                    ++this.count;
                    return true;
                }
            } else {
                TValue valueNode = new TValue();
                valueNode.build(path[index], FileSystemNodeType.INTERMEDIATE);
                TNode searchNode = new TNode();
                searchNode.Value = valueNode;

                TNode folder;

                if (node.children.Search(searchNode, out folder)) {
                    return this.addPath(folder, path, ++index, ref newNode);
                } else {
                    searchNode.parent = node;
                    node.children.Insert(searchNode, false);
                    ++this.count;
                    return this.addPath(searchNode, path, ++index, ref newNode);
                }
            }
        }

        /// <summary>
        /// Search a node and returns true if and only if the node was found.
        /// </summary>
        /// <param name="path">Path to match</param>
        /// <param name="type">Type to match</param>
        /// <param name="searchedNode">Searched node</param>
        /// <returns>True if and only if the node was found</returns>
        public virtual bool Search(String path, FileSystemNodeType type, out TNode searchedNode)
        {
            String[] pathElems = path.Split(this.separator);
            return this.search(this.root, pathElems, FIRST_INDEX, type, out searchedNode);
        }

        /// <summary>
        /// Search a node and returns true if and only if the node was found.
        /// </summary>
        /// <param name="node">Node where the search will be done</param>
        /// <param name="path">Path elements to match</param>
        /// <param name="index">Index of the current path element</param>
        /// <param name="type">Type to match</param>
        /// <param name="searchedNode">Searched node</param>
        /// <returns>True if and only if the node was found</returns>
        protected virtual bool search(TNode node, String[] path, int index, FileSystemNodeType type, out TNode searchedNode)
        {
            TValue valueNode = new TValue();
            TNode searchNode = new TNode();

            if (index == path.Length - 1) {
                valueNode.build(path[index], type);
                searchNode.Value = valueNode;

                if (node.children.Search(searchNode, out searchNode)) {
                    searchedNode = searchNode;
                    return true;
                } else {
                    searchedNode = null;
                    return false;
                }
            } else {
                valueNode.build(path[index], FileSystemNodeType.INTERMEDIATE);
                searchNode.Value = valueNode;
                if (node.children.Search(searchNode, out searchNode))
                    return this.search(searchNode, path, ++index, type, out searchedNode);
                else {
                    searchedNode = null;
                    return false;
                }
            }
        }

        /// <summary>
        /// Deletes a path in the tree. Children nodes of the node asociated to the path will be also deleted.
        /// </summary>
        /// <param name="path">Path to match</param>
        /// <param name="type">Type to match</param>
        /// <returns>True if and only if the node was deleted</returns>
        public virtual bool DeletePath(String path, FileSystemNodeType type)
        {
            TNode node;

            if (this.Search(path, type, out node)) {
                if (type == FileSystemNodeType.LEAF) {
                    //Delete node
                    node.parent.children.Delete(node);
                    --this.count;

                } else if (type == FileSystemNodeType.INTERMEDIATE) {
                    //Cascade down
                    foreach (FileSystemTreeNode child in node as IEnumerable<FileSystemTreeNode>)
                        --this.count;

                    //Delete node
                    node.parent.children.Delete(node); //How foolish can be GC?
                } else
                    throw new ArgumentException("Unexpected node type");

                return true;
            } else
                return false;
        }

        /// <summary>
        /// Deletes a path in the tree. Children nodes of the node asociated to the path will be also deleted.
        /// </summary>
        /// <param name="path">Path to match</param>
        /// <param name="type">Type to match</param>
        /// <returns>True if and only if the node was deleted</returns>
        public virtual bool DeletePath(String path, FileSystemNodeType type, ref TNode deleted)
        {
            TNode node;

            if (this.Search(path, type, out node)) {
                if (type == FileSystemNodeType.LEAF) {
                    //Delete node
                    node.parent.children.Delete(node);
                    --this.count;

                } else if (type == FileSystemNodeType.INTERMEDIATE) {
                    //Cascade down
                    foreach (FileSystemTreeNode child in node as IEnumerable<FileSystemTreeNode>)
                        --this.count;

                    //Delete node
                    deleted = node;
                    deleted.children = null; //How foolish can be GC?
                    node.parent.children.Delete(node);
                } else
                    throw new ArgumentException("Unexpected node type");

                return true;
            } else
                return false;
        }

        #endregion

        #region ICOLLECTION

        public override void Add(TNode node)
        {
            String[] pathElems = node.Value.name.Split(this.separator);
            TValue newValue = new TValue();
            newValue.build(pathElems[pathElems.Length - 1], node.Value.type);
            node.Value = newValue;

            this.addPath(this.root, pathElems, FIRST_INDEX, ref node);
        }

        public override bool Contains(TNode node)
        {
            String[] pathElems = node.Value.name.Split(this.separator);
            TValue newValue = new TValue();
            newValue.build(pathElems[pathElems.Length - 1], node.Value.type);
            node.Value = newValue;

            TNode searchedNode;
            return this.search(this.root, pathElems, FIRST_INDEX, node.Value.type, out searchedNode);
        }

        public override bool Remove(TNode node)
        {
            return this.DeletePath(node.Value.name, node.Value.type);
        }

        #endregion
    }
    
}
