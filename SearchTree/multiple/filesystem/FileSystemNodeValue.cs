﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-05
 SPECIAL NOTES:
 VERSION: 1.2
====================================
 Change History:

    VERSION 1.1: Added build() method.
    VERSION 1.2: The class is now public
==================================*/

using System;

namespace SearchTree.multiple.filesystem
{
    public class FileSystemNodeValue : IComparable<FileSystemNodeValue>
    {
        #region FIELDS

        /// <summary>
        /// Node´s name
        /// </summary>
        internal String name;

        /// <summary>
        /// Node´s type
        /// </summary>
        internal FileSystemNodeType type;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Node´s name
        /// </summary>
        public String Name { get { return this.name; } }

        /// <summary>
        /// Node´s type
        /// </summary>
        public FileSystemNodeType Type { get { return this.type; } }

        #endregion

        public FileSystemNodeValue()
        {
            this.build(String.Empty, FileSystemNodeType.INTERMEDIATE);
        }

        public FileSystemNodeValue(String name, FileSystemNodeType type)
        {
            this.build(name, type);
        }

        #region ICOMPARABLE

        /// <summary>
        /// Compares this value with another value.
        /// </summary>
        /// <param name="value">Value to compare with this value</param>
        /// <returns>
        ///     A value less than zero if this instance precedes obj in the sort order.
        ///     Zero if this instance occurs in the same position in the sort order as obj.
        ///     A value greater than zero if this instance follows obj in the sort order.
        ///</returns>
        public int CompareTo(FileSystemNodeValue value)
        {
            int nameC = this.name.CompareTo(value.name);

            if (nameC == 0) {
                return this.type.CompareTo(value.type);
            } else {
                return nameC;
            }
        }

        #endregion

        public virtual void build(String name, FileSystemNodeType type)
        {
            this.name = name;
            this.type = type;
        }
    }
}
