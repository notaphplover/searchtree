﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:
==================================*/
namespace SearchTree
{
    public interface ITree<TNode, TValue>
        where TNode : ITreeNode<TNode, TValue>
    {
        #region PROPERTIES

        /// <summary>
        /// Number of elements in the tree
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Root node. This node could be a leaf node
        /// </summary>
        TNode Root { get; }

        #endregion
    }
}
