﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:
==================================*/
namespace SearchTree
{
    public interface ITreeNode<TNode, TValue>
        where TNode : ITreeNode<TNode, TValue>
    {
        #region PROPERTIES

        TValue Value { get; set; }

        #endregion
    }
}
