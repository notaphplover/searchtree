#### COPYRIGHT PARAGRAPHS ####

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

#### END COPYRIGHT PARAGRAPHS ####

This library is designed to use multiple trees and develop easily new ones.

If you are a user of this library, you should know that:

1. You can navigate throught the trees and modify its attributes, but you 
could corrupt the trees if you do the wrong changes. This libray is designed
to be used by users that knows the algorithms that are inside the trees. If you
don�t know the algoritms you still will be albe to call all the public methods 
of a tree.

If you want to add new trees to the library, please consider the following aspects:

1. There is no definition of the insertion, search and deletion methods. You are
free to implement the methods as you desire, but we try to implement that methods 
in this way.

	Insert: The returning type should be allways a bool indicating the success /
	failure of the operation. At least one of the implementations of this 
	method should have a parameter that returns the inserted node. If the node 
	is not inserted (because you decided that the nodes won�t be overwritten), the
	previous parameter could return an interesting node of the tree (for example,
	AATree<TKey>.Insert(TKey key, bool overwrite, out AANode<TKey> inserted) would
	return the node with an equivalent key in the tree).

	Search: The returning type should be allways a bool indicating the success /
	failure of the operation. One of the function parameters should return the 
	searched node / value.

	Delete: The returning type should be allways a bool indicating the success /
	failure of the operation. One of the function parameters should return the 
	deleted node / value.

	This is not a law because there are trees that can not be implemented in that
	way, but this is the desired way of implementing tree methods.

2. The tree should be implemented to allow the user to access to almost every
interesting element of the tree. This library is designed to be used by users that
knows the tree algorithms. If a user wants to modify the nodes of the tree, he can 
under his own risk. If the user really knows the algorithm, there should be no
problems around this.

3. All the public methods implemented in a tree should never corrupt an stable tree.

Thak you for reading this. I still don�t know where i�ll update this repository, but 
feel free to ask me everything you want. You can contact me writing an e-mail to the
following address: roberto.pintos.lopez@gmail.com.

Let�s have fun building this together :)