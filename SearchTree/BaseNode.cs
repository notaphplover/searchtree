﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:
 
    VERSION 1.1: Fixed getEnumerator() method.
==================================*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace SearchTree
{
    public abstract class BaseNode<TNode, TValue> : ITreeNode<TNode, TValue>, IEnumerable<TNode>
        where TNode : BaseNode<TNode, TValue>
    {
        #region PROPERTIES

        /// <summary>
        /// Node´s inner value
        /// </summary>
        public abstract TValue Value { get; set; }

        /// <summary>
        /// Children nodes.
        /// </summary>
        public abstract IEnumerable<TNode> Children { get; }

        #endregion

        #region IENUMERABLE

        internal virtual IEnumerator<TNode> getEnumerator()
        {
            if (this is TNode)
                yield return this as TNode;
            else
                throw new Exception();

            foreach (TNode element in this.Children) {
                if (element != null) {
                    IEnumerator<TNode> subElements = element.getEnumerator();

                    while (subElements.MoveNext()) {
                        if (subElements.Current != null)
                            yield return subElements.Current;
                    }
                }
            }
        }

        IEnumerator<TNode> IEnumerable<TNode>.GetEnumerator() 
        {
            return this.getEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.getEnumerator();
        }

        #endregion
    }
}
