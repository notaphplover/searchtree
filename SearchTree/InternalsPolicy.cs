﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-01
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:
 
    VERSION 1.1: Added SearchTreeTest project
==================================*/
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TestProject")]
[assembly: InternalsVisibleTo("SearchTreeTest")]
namespace Procorlib { }

