﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-05
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:
 
    VERSION 1.1: Fixed getValuesEnumerator() method 
==================================*/
using System.Collections;
using System.Collections.Generic;

namespace SearchTree
{
    public abstract class ValueEnumerableTree<TNode, TValue> : BaseTree<TNode, TValue>, IEnumerable<TValue>
        where TNode : BaseNode<TNode, TValue>
    {
        public ValueEnumerableTree() 
            : base() 
        { }

        public ValueEnumerableTree(TNode root)
            : base(root)
        { }

        #region IENUMERABLE

        protected virtual IEnumerator<TValue> getValuesEnumerator()
        {
            IEnumerator<TNode> iterator = this.getNodesEnumerator();

            while (iterator.MoveNext()) {
                yield return iterator.Current.Value;
            } 
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.getNodesEnumerator();
        }

        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return this.getValuesEnumerator();
        }

        #endregion
    }
}
