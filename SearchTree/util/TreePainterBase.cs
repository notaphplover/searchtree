﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchTree.util
{
    public abstract class TreePainterBase
    {
        protected abstract void paintNode<TNode, TValue>(Graphics graphics, TNode node, Point offSet)
            where TNode : BaseNode<TNode, TValue>
        ;

        protected abstract Size getNodeRectangle();

        public void Paint<TTree, TNode, TValue>(Graphics graphics, TTree tree)
            where TTree : BaseTree<TNode, TValue>
            where TNode : BaseNode<TNode, TValue>
        {
            Queue<TNode> nodesQueue = new Queue<TNode>();
            //x: offset-x y: offset-y in nodes-units
            Queue<Point> offSets = new Queue<Point>();

            Stack<TNode> nodesStack = new Stack<TNode>();

            //x: level. y: 0 if the node was not processed.
            Stack<Point> nodesInfo = new Stack<Point>();

            int maxLevel = 0;

            nodesStack.Push(tree.Root);
            nodesInfo.Push(new Point(0, 0));

            //Loop procedure
            while (nodesStack.Count > 0) {
                Point nodeInfo = nodesInfo.Pop();

                if (nodeInfo.Y == 0) { 
                    nodeInfo.Y = 1;
                    nodesInfo.Push(nodeInfo);
                    TNode node = nodesStack.Peek();

                    IEnumerable<TNode> children = node.Children;

                    if (children.Any() && maxLevel < nodeInfo.X + 1) maxLevel = nodeInfo.X + 1;

                    foreach (TNode child in children) {
                        nodesStack.Push(child);
                        nodesInfo.Push(new Point(nodeInfo.X + 1, 0));
                    }
                } else {
                    nodesQueue.Enqueue(nodesStack.Pop());
                    offSets.Enqueue(new Point(nodeInfo.X, nodesQueue.Count));
                }

                TNode pivot = nodesStack.Peek();
            }

            //Draw nodes
            while (nodesQueue.Count > 0) {
                this.paintNode<TNode, TValue>(graphics, nodesQueue.Dequeue(), offSets.Dequeue()); 
            }
        }
    }
}
