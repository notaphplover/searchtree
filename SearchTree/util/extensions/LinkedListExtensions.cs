﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchTree.util.extensions
{
    internal static class LinkedListExtensions
    {
        public static T ElementAt<T>(this LinkedList<T> obj, int index)
        {
            if (index < 0)
                throw new IndexOutOfRangeException();

            for (LinkedListNode<T> child = obj.First; child != null; child = child.Next) {
                if (index-- == 0)
                    return child.Value;
            }

            throw new IndexOutOfRangeException();
        }
    }
}
