﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SearchTree.binary.balanced;

namespace SearchTreeTest
{
    [TestClass]
    public class BTreeSentinelTest
    {
        internal const String AATREE_SENTINEL_NAME = "sentinel";

        internal const int NUMBER_OF_VALUES = 10000;

        internal AANode<TKey, TValue> getSentinel<TKey, TValue>(AATree<TKey, TValue> tree) where TKey : IComparable<TKey>
        {
            PrivateObject obj = new PrivateObject(tree);
            return obj.GetField(AATREE_SENTINEL_NAME) as AANode<TKey, TValue>;
        }

        [
            TestMethod,
            TestCategory("BinaryTree"),
        ]
        public void BTreeSentinelMustNotReturnSentinels() 
        {
            AATree<int, int> tree = new AATree<int, int>();

            AANode<int, int> sentinel = this.getSentinel<int, int>(tree);

            int numValues = NUMBER_OF_VALUES / 10;

            int[] values = new int[numValues];

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                values[i] = randomGen.Next();
                tree.Insert(i, values[i], true);
            }

            int nodes = 0;

            //Check that the foreach does not return sentinels
            foreach (AANode<int, int> node in tree as IEnumerable<AANode<int, int>>) {
                ++nodes;
                Assert.IsTrue(node != sentinel);
            }

            Assert.AreEqual(tree.Count, nodes);
        }

        [
            TestMethod,
            TestCategory("BinaryTree"),
        ]
        public void BTreeSentinelMustGetViewBetween()
        {
            AATree<int, int> tree = new AATree<int, int>();

            int numValues = NUMBER_OF_VALUES;

            int[] values = new int[numValues];

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i)
            {
                values[i] = randomGen.Next();
                tree.Insert(i, values[i], true);
            }

            //GetView Call
            int minimun = 1000;
            int pivot = minimun + (int)(randomGen.NextDouble() * (numValues - minimun));

            //Case A: Get the firsts ones
            LinkedList<AANode<int, int>> items = tree.getViewBetween(minimun, pivot);

            Assert.AreEqual(pivot - minimun + 1, items.Count);

            foreach (AANode<int, int> node in items)
            {
                Assert.IsTrue(node.key >= minimun && node.key <= pivot);
            }

            //Case B: Get the last ones
            items = tree.getViewBetween(pivot, numValues);

            Assert.AreEqual(numValues - pivot, items.Count);

            foreach (AANode<int, int> node in items)
            {
                Assert.IsTrue(node.key >= pivot && node.key <= numValues);
            }
        }
    }
}
