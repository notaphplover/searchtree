﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-07-29
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SearchTree.binary.balanced;

namespace SearchTreeTest
{
    [TestClass]
    public class AATreeTest
    {
        internal const String AATREE_SENTINEL_NAME = "sentinel";

        internal const int NUMBER_OF_VALUES = 10000;

        internal TNode getSentinel<TKey, TNode, TValue>(AATreeBase<TKey, TNode, TValue> tree) 
            where TKey : IComparable<TKey>
            where TNode : AANodeBase<TKey, TNode, TValue>, new()
        {
            PrivateObject obj = new PrivateObject(tree);
            return obj.GetField(AATREE_SENTINEL_NAME) as TNode;
        }

        internal bool isAATreeBalanced<TKey, TNode, TValue>(AATreeBase<TKey, TNode, TValue> tree)
            where TKey : IComparable<TKey>
            where TNode : AANodeBase<TKey, TNode, TValue>, new()
        {
            TNode sentinel = this.getSentinel<TKey, TNode, TValue>(tree);

            foreach (TNode node in tree as IEnumerable<TNode>) {
                //1. The level of every leaf node is one.
                if (node.isLeaf()) {
                    //Sentinel nodes have a level equal to zero
                    if (node == sentinel) {
                        if (node.level != 0) return false;
                        continue;
                    } else {
                        if (node.level != 1) return false;
                    }
                }
                //2. The level of every left child is exactly one less than that of its parent
                if (node.left != sentinel) {
                    if (node.left.level + 1 != node.level) return false;
                }
                //3. The level of every right child is equal to or one less than that of its parent
                if (node.right != sentinel) {
                    if (node.right.level != node.level && node.right.level + 1 != node.level) return false;
                }
                //4. The level of every right grandchild is strictly less than that of its grandparent.
                if (node.right != sentinel && node.right.right != sentinel) {
                    //No need to check left children. They have been checked on 3
                    if (node.right.right.level >= node.level) return false;
                }
                //5. Every node of level greater than one has two children
                if (node.level > 1 && (node.left == sentinel || node.right == sentinel)) return false;
            }
            return true;
        }

        internal void displayInfo<TKey, TNode, TValue>(AATreeBase<TKey, TNode, TValue> tree)
            where TKey : IComparable<TKey>
            where TNode : AANodeBase<TKey, TNode, TValue>, new()
        {
            int nonSentinel = 0;
            int sentinel = 0;
            int maxLevel = 0;

            TNode sentinelNode = this.getSentinel<TKey, TNode, TValue>(tree);

            foreach (TNode node in tree as IEnumerable<TNode>) {
                if (node == sentinelNode) {
                    ++sentinel;
                } else {
                    ++nonSentinel;

                    if (node.level > maxLevel) maxLevel = node.level;
                }
            }

            Assert.AreEqual(tree.Count, nonSentinel);

            Console.WriteLine("Non sentinel appearances: " + nonSentinel);
            Console.WriteLine("Sentinel appearances: " + sentinel);
            Console.WriteLine("Maximun level: " + maxLevel);
        }

        [
            TestMethod,
            TestCategory("AATree"),
        ]
        public void AATreeMustInsertAndDeleteMyElementsI()
        {
            AATree<int, int> tree = new AATree<int, int>();

            int numValues = NUMBER_OF_VALUES;

            int[] values = new int[numValues];

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                values[i] = randomGen.Next();
                tree.Insert(i, values[i], true);
            }

            Assert.AreEqual(numValues, tree.Count);

            //Search elements
            for (int i = 0; i < numValues; ++i) {
                int result;

                if (!tree.Search(i, out result))
                    Assert.Fail();

                Assert.AreEqual(values[i], result);
            }

            Console.WriteLine("Inserted nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));

            //Delete left elements

            for (int i = 0; i < numValues / 4; ++i) {
                int valueDeleted;
                tree.Delete(i, out valueDeleted);

                Assert.AreEqual(values[i], valueDeleted);
            }

            Assert.AreEqual(tree.Count, numValues - numValues / 4);

            Console.WriteLine("Deleted left nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));

            //Delete right elements

            int finalIndex = numValues * 3 / 4;

            for (int i = numValues / 2; i < finalIndex; ++i) {
                int valueDeleted;
                tree.Delete(i, out valueDeleted);

                Assert.AreEqual(values[i], valueDeleted);
            }

            Assert.AreEqual(numValues / 2, tree.Count);

            Console.WriteLine("Deleted right nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));
        }

        [
            TestMethod,
            TestCategory("AATree"),
        ]
        public void AATreeMustInsertAndDeleteMyElementsII()
        {
            AATree<int, int> tree = new AATree<int, int>();

            int numValues = NUMBER_OF_VALUES;

            int[] values = new int[numValues];

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                values[i] = randomGen.Next();
                tree.Insert(-i, values[i], true);
            }

            Assert.AreEqual(numValues, tree.Count);

            //Search elements
            for (int i = 0; i < numValues; ++i) {
                int result;

                if (!tree.Search(-i, out result))
                    Assert.Fail();

                Assert.AreEqual(values[i], result);
            }

            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));

            //Delete left elements

            int finalIndex = numValues * 3 / 4;

            for (int i = numValues / 2; i < finalIndex; ++i) {
                int valueDeleted;
                tree.Delete(-i, out valueDeleted);

                Assert.AreEqual(values[i], valueDeleted);
            }

            Assert.AreEqual(numValues - numValues / 4, tree.Count);

            Console.WriteLine("Deleted left nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));

            //Delete right elements

            for (int i = 0; i < numValues / 4; ++i) {
                int valueDeleted;
                tree.Delete(-i, out valueDeleted);

                Assert.AreEqual(values[i], valueDeleted);
            }

            Assert.AreEqual(numValues / 2, tree.Count);

            Console.WriteLine("Deleted right nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int, int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int, int>, int>(tree));
        }

        [
            TestMethod,
            TestCategory("AATree"),
        ]
        public void SimpleAATreeMustInsertAndDeleteMyElementsI()
        { 
            AATree<int> tree = new AATree<int>();

            int numValues = NUMBER_OF_VALUES;

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                tree.Insert(i, true);
            }

            Assert.AreEqual(numValues, tree.Count);

            //Search elements
            for (int i = 0; i < numValues; ++i) {
                int result;

                if (!tree.Search(i, out result))
                    Assert.Fail();
            }

            Console.WriteLine("Inserted nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));

            //Delete left elements

            for (int i = 0; i < numValues / 4; ++i)
            {
                int valueDeleted;
                tree.Delete(i, out valueDeleted);

                Assert.AreEqual(i, valueDeleted);
            }

            Assert.AreEqual(tree.Count, numValues - numValues / 4);

            Console.WriteLine("Deleted left nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));

            //Delete right elements

            int finalIndex = numValues * 3 / 4;

            for (int i = numValues / 2; i < finalIndex; ++i)
            {
                int valueDeleted;
                tree.Delete(i, out valueDeleted);

                Assert.AreEqual(i, valueDeleted);
            }

            Assert.AreEqual(numValues / 2, tree.Count);

            Console.WriteLine("Deleted right nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));
        }

        [
            TestMethod,
            TestCategory("AATree"),
        ]
        public void SimpleAATreeMustInsertAndDeleteMyElementsII()
        { 
            AATree<int> tree = new AATree<int>();

            int numValues = NUMBER_OF_VALUES;

            Random randomGen = new Random(DateTime.Now.Millisecond);

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                tree.Insert(-i, true);
            }

            Assert.AreEqual(numValues, tree.Count);

            //Search elements
            for (int i = 0; i < numValues; ++i) {
                int result;

                if (!tree.Search(-i, out result))
                    Assert.Fail();

                Assert.AreEqual(-i, result);
            }

            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));

            //Delete left elements

            int finalIndex = numValues * 3 / 4;

            for (int i = numValues / 2; i < finalIndex; ++i) {
                int valueDeleted;
                tree.Delete(-i, out valueDeleted);

                Assert.AreEqual(-i, valueDeleted);
            }

            Assert.AreEqual(numValues - numValues / 4, tree.Count);

            Console.WriteLine("Deleted left nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));

            //Delete right elements

            for (int i = 0; i < numValues / 4; ++i) {
                int valueDeleted;
                tree.Delete(-i, out valueDeleted);

                Assert.AreEqual(-i, valueDeleted);
            }

            Assert.AreEqual(numValues / 2, tree.Count);

            Console.WriteLine("Deleted right nodes.\n--------------------");
            //Display relevant information
            this.displayInfo<int, AANode<int>, int>(tree);

            //Check tree status
            Assert.IsTrue(this.isAATreeBalanced<int, AANode<int>, int>(tree));
        }

        [
            TestMethod,
            TestCategory("AATree"),
        ]
        public void AATreeMustReturnMyInsertedElements()
        { 
            AATree<int, int> tree = new AATree<int, int>();

            int numValues = NUMBER_OF_VALUES;

            int[] values = new int[numValues];

            Random randomGen = new Random(DateTime.Now.Millisecond);

            int maximun = Int32.MaxValue - 1;

            //Insert elements
            for (int i = 0; i < numValues; ++i) {
                AANode<int, int> insertedNode;
                values[i] = randomGen.Next(maximun);
                tree.Insert(-i, values[i], true, out insertedNode);

                Assert.AreEqual(values[i], insertedNode.Value);
            }

            //Insert elements II
            for (int i = 0; i < numValues; ++i) {
                AANode<int, int> insertedNode;
                tree.Insert(-i, values[i] + 1, false, out insertedNode);

                Assert.AreEqual(values[i], insertedNode.Value);
            }

            //Insert elements III
            for (int i = 0; i < numValues; ++i) {
                AANode<int, int> insertedNode;
                tree.Insert(-i, values[i] + 1, true, out insertedNode);

                Assert.AreEqual(values[i] + 1, insertedNode.Value);
            }
        }
    }
}
