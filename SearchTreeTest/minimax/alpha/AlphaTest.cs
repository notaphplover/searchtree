﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SearchTreeTest.minimax.alpha
{
    [TestClass]
    public class AlphaTest
    {
        [
            TestMethod,
            TestCategory("Alpha prune"),
        ]
        public void AlphaTreeMustBeInitializable()
        {
            int factor = 2;
            int players = 2;

            AlphaTreeTest tree = new AlphaTreeTest(
                new AlphaPruneRandomNodeGenerator(factor),
                new AlphaPruneNodeTest(players, new MinimaxValueTest(0, players)),
                players
            );
        }

        [
            TestMethod,
            TestCategory("Alpha prune"),
        ]
        public void AlphaTreeMustGenerateNodesAndGetMinimaxTrace()
        {
            int factor = 2;
            int players = 2;

            int[] controlers = new int[] { 0, 1, 0, 1, 0, 1, 0, 1 };

            AlphaTreeTest tree = new AlphaTreeTest(
                new AlphaPruneRandomNodeGenerator(factor),
                new AlphaPruneNodeTest(players, new MinimaxValueTest(0, players)),
                players
            );

            //Generate trace
            AlphaPruneNodeTest[] trace = tree.getTrace(controlers);

            Assert.IsTrue(
                Array.TrueForAll<AlphaPruneNodeTest>(
                    trace,
                    new Predicate<AlphaPruneNodeTest>(
                        (AlphaPruneNodeTest node) => { return node != null; }
                    )
                ) 
            );
        }
    }
}
