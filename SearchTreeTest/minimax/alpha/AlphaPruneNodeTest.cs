﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using SearchTree.multiple.minimax.alpha;

namespace SearchTreeTest.minimax.alpha
{
    internal class AlphaPruneNodeTest : AlphaPruneTreeBaseNode<AlphaPruneNodeTest, MinimaxValueTest>
    {
        #region ATTRIBUTES

        protected MinimaxValueTest value;

        #endregion

        #region PROPERTIES

        public override MinimaxValueTest Value { get { return this.value; } set { this.value = value; } }

        #endregion

        public AlphaPruneNodeTest(int players)
            : base(players)
        { }

        public AlphaPruneNodeTest(int players, MinimaxValueTest value)
            : base(players)
        {
            this.value = value;
        }

        public AlphaPruneNodeTest(int players, MinimaxValueTest value, AlphaPruneNodeTest parent)
            : base(players, parent)
        {
            this.value = value;
        }
    }
}
