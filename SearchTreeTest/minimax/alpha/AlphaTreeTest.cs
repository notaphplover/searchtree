﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using SearchTree.multiple.minimax;
using SearchTree.multiple.minimax.alpha;

namespace SearchTreeTest.minimax.alpha
{
    internal class AlphaTreeTest : AlphaPruneTreeBase<AlphaPruneNodeTest, MinimaxValueTest>
    {
        public AlphaTreeTest(IMinimaxGenerator<AlphaPruneNodeTest, MinimaxValueTest> comparer, AlphaPruneNodeTest root, int players)
            : base(comparer, root, players)
        { }
    }
}
