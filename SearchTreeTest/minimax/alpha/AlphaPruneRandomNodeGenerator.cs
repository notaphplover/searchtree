﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-20
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: method generateNodes returns now IEnumerator<AlphaPruneNodeTest>
==================================*/
using System;
using System.Collections.Generic;

using SearchTree.multiple.minimax;

namespace SearchTreeTest.minimax.alpha
{
    internal class AlphaPruneRandomNodeGenerator : IMinimaxGenerator<AlphaPruneNodeTest, MinimaxValueTest>
    {
        protected int factor;

        protected Random random;

        public AlphaPruneRandomNodeGenerator(int factor)
        {
            this.factor = factor;
            this.random = new Random(DateTime.Now.Millisecond);
        }

        public IEnumerator<AlphaPruneNodeTest> generateNodes(AlphaPruneNodeTest node)
        {
            for (int i = 0; i < factor; ++i)
                yield return new AlphaPruneNodeTest(node.Players, new MinimaxValueTest(this.random.Next(), node.Players));
        }
    }
}
