﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-15
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using SearchTree.multiple.minimax;

namespace SearchTreeTest.minimax
{
    internal class MinimaxTreeTest : MinimaxTreeBase<MinimaxNodeTest, MinimaxValueTest>
    {
        public MinimaxTreeTest(IMinimaxGenerator<MinimaxNodeTest, MinimaxValueTest> comparer, MinimaxNodeTest root, int players)
            : base(comparer, root, players)
        { }
    }
}
