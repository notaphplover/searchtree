﻿
//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-15
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: method generateNodes returns now IEnumerator<MinimaxNodeTest>
==================================*/
using System;
using System.Collections.Generic;

using SearchTree.multiple.minimax;

namespace SearchTreeTest.minimax
{
    internal class MinimaxRandomNodeGenerator : IMinimaxGenerator<MinimaxNodeTest, MinimaxValueTest>
    {
        protected int factor;

        protected Random random;

        public MinimaxRandomNodeGenerator(int factor)
        {
            this.factor = factor;
            this.random = new Random(DateTime.Now.Millisecond);
        }

        public IEnumerator<MinimaxNodeTest> generateNodes(MinimaxNodeTest node)
        {
            for (int i = 0; i < factor; ++i) 
                yield return new MinimaxNodeTest(node.Players, new MinimaxValueTest(this.random.Next(), node.Players));
        }
    }
}
