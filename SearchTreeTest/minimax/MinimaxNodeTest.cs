﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-15
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using SearchTree.multiple.minimax;

namespace SearchTreeTest.minimax
{
    internal class MinimaxNodeTest : MinimaxTreeNodeBase<MinimaxNodeTest, MinimaxValueTest>
    {
        #region ATTRIBUTES

        protected MinimaxValueTest value;

        #endregion

        #region PROPERTIES

        public override MinimaxValueTest Value { get { return this.value; } set { this.value = value; } }

        #endregion

        public MinimaxNodeTest (int players)
            : base(players)
        { }

        public MinimaxNodeTest(int players, MinimaxValueTest value)
            : base(players)
        {
            this.value = value;
        }
    }
}
