﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-15
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SearchTreeTest.minimax
{
    [TestClass]
    public class MinimaxTest
    {
        [
            TestMethod,
            TestCategory("Minimax"),
        ]
        public void MinimaxTreeMustBeInitializable()
        {
            int factor = 2;
            int players = 2;
            MinimaxTreeTest tree = new MinimaxTreeTest(
                new MinimaxRandomNodeGenerator(factor), 
                new MinimaxNodeTest(players, new MinimaxValueTest(0, players)),
                players
            );
        }

        [
            TestMethod,
            TestCategory("Minimax"),
        ]
        public void MinimaxTreeMustGenerateNodesAndGetMinimaxTrace()
        {
            int factor = 2;
            int players = 2;
            int levels = 7;

            int[] controlers = new int[] { 0, 1, 0, 1, 0, 1, 0, 1 };

            MinimaxTreeTest tree = new MinimaxTreeTest(
                new MinimaxRandomNodeGenerator(factor),
                new MinimaxNodeTest(players, new MinimaxValueTest(0, players)),
                players
            );

            //Generate trace
            MinimaxNodeTest[] trace = tree.getTrace(controlers);

            Assert.AreEqual((Math.Pow(factor, levels + 1) - 1) / (factor - 1), tree.Count);

            Assert.IsTrue(
                Array.TrueForAll<MinimaxNodeTest>(
                    trace, 
                    new Predicate<MinimaxNodeTest>(
                        (MinimaxNodeTest node) => { return node != null; }
                    )
                )
            );
        }
    }
}
