﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-15
 SPECIAL NOTES:
 VERSION: 1.0
====================================
 Change History:

==================================*/
using SearchTree.multiple.minimax;

namespace SearchTreeTest.minimax
{
    internal class MinimaxValueTest : IMinimaxNodeVale
    {
        #region ATTRIBUTES

        protected int value;

        protected int[] values;

        #endregion

        #region PROPERTIES

        #endregion

        public MinimaxValueTest(int value, int players)
        {
            this.value = value;

            this.values = new int[players];

            for (int i = 0; i < players; ++i) {
                if (i % 2 == 0)
                    this.values[i] = value;
                else
                    this.values[i] = -value;
            }
        }

        /// <summary>
        /// Returns the heuristic value of this node associated with a player
        /// </summary>
        /// <param name="player">Player´s value</param>
        /// <returns>Player´s heuristic value of this node.</returns>
        public int getValue(int player)
        {
            return value;
        }

        /// <summary>
        /// Returns the vector of heuristic values of this node associated with each player.
        /// </summary>
        /// <returns>Vector of heuristic values of this node associated with each player.</returns>
        public int[] getValues()
        {
            return this.values;
        }
    }
}
