﻿//Clasificación del archivo: RESTRINGIDA
/*=================================
 AUTHORS:
            Roberto Pintos López

 CREATE DATE: 2016-08-10
 SPECIAL NOTES:
 VERSION: 1.1
====================================
 Change History:

    VERSION 1.1: Added FileSystemIntermediateNodesMustBeIntermediate() and FileSystemTreeMustReturnMyNodesAndValues() methods.
==================================*/
using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SearchTree.multiple.filesystem;

namespace SearchTreeTest
{
    [TestClass]
    public class FileSystemTreeTest
    {
        [
            TestMethod,
            TestCategory("FileSystemTree"),
        ]
        public void FileSystemMustBeInitializable()
        {
            FileSystemTree tree = new FileSystemTree();
        }

        [
            TestMethod,
            TestCategory("FileSystemTree"),
        ]
        public void FileSystemMustWriteFoldersAndFiles()
        {
            String[] paths = new String[] { 
                //Insert test file paths here
                "C:\\random\\path\\folder",
                "C:\\random\\path\\file",
                "D:\\anything\\can\\happen",
                "D:\\anything\\can\\happen",
            };

            FileSystemNodeType[] types = new FileSystemNodeType[] {
                //Insert test file types here
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
            };

            if (paths.Length != types.Length)
                Assert.Fail("There must be as many paths as types");

            int nodes = paths.Length;

            FileSystemTree tree = new FileSystemTree();

            for (int i = 0; i < nodes; ++i) {
                Assert.IsTrue(tree.AddPath(paths[i], types[i]));
            }

            int elements = tree.Count;

            Assert.IsTrue(elements >= nodes);

            //Insert the existing nodes and check that the counter remains at the same value
            for (int i = 0; i < nodes; ++i) {
                Assert.IsFalse(tree.AddPath(paths[i], types[i]));
            }

            Assert.IsTrue(elements == tree.Count);

            //Assert that each node is found
            for (int i = 0; i < nodes; ++i) { 
                FileSystemTreeNode node;
                Assert.IsTrue(tree.Search(paths[i], types[i], out node));
                Assert.IsTrue(node.Value.Type == types[i]);
                String[] path = paths[i].Split(FileSystemTree.DEFAULT_SEPARATOR);
                Assert.IsTrue(node.Value.Name == path[path.Length - 1]);
            }
        }

        [
            TestMethod,
            TestCategory("FileSystemTree"),
        ]
        public void FileSystemIntermediateNodesMustBeIntermediate()
        { 
            String[] paths = new String[] { 
                //Insert test file paths here
                "C:\\random\\path\\folder",
                "C:\\random\\path\\file",
                "D:\\anything\\can\\happen",
                "D:\\anything\\can\\happen",
            };

            FileSystemNodeType[] types = new FileSystemNodeType[] {
                //Insert test file types here
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
            };

            if (paths.Length != types.Length)
                Assert.Fail("There must be as many paths as types");

            int nodes = paths.Length;

            FileSystemTree tree = new FileSystemTree();

            for (int i = 0; i < nodes; ++i) {
                Assert.IsTrue(tree.AddPath(paths[i], types[i]));
            }

            foreach(FileSystemTreeNode node in tree as IEnumerable<FileSystemTreeNode>) {
                if (node.children.Count > 0 && node.Value.type == FileSystemNodeType.LEAF)
                    Assert.Fail();
            }
        }

        [
            TestMethod,
            TestCategory("FileSystemTree"),
        ]
        public void FileSystemTreeMustReturnMyNodesAndValues()
        {
            String[] paths = new String[] { 
                //Insert test file paths here
                "C:\\random\\path\\folder",
                "C:\\random\\path\\file",
                "D:\\anything\\can\\happen",
                "D:\\anything\\can\\happen",
            };

            FileSystemNodeType[] types = new FileSystemNodeType[] {
                //Insert test file types here
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
                FileSystemNodeType.INTERMEDIATE,
                FileSystemNodeType.LEAF,
            };

            if (paths.Length != types.Length)
                Assert.Fail("There must be as many paths as types");

            int nodes = paths.Length;

            FileSystemTree tree = new FileSystemTree();

            for (int i = 0; i < nodes; ++i) {
                FileSystemTreeNode inserted = null;
                Assert.IsTrue(tree.AddPath(paths[i], types[i], ref inserted));

                Assert.AreEqual(types[i], inserted.Value.Type);

                String[] pathParts = paths[i].Split(FileSystemTree.DEFAULT_SEPARATOR);
                Assert.AreEqual(pathParts[pathParts.Length - 1], inserted.Value.Name);
            }

            int nodesCount = 0;

            foreach (FileSystemTreeNode node in tree as IEnumerable<FileSystemTreeNode>) {
                if (tree.Root != node)
                    ++nodesCount;
            }

            Assert.AreEqual(tree.Count, nodesCount);

            nodesCount = 0;

            foreach (FileSystemNodeValue value in tree as IEnumerable<FileSystemNodeValue>) {
                ++nodesCount;
            }

            Assert.AreEqual(tree.Count, nodesCount);
        }
    }
}
